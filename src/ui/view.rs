use std::{cmp, collections::HashMap};

use chrono::Local;
use nekochat::api::{Message, User, UserId};
use tuja::{Terminal, Panel, Color16, Color, Formatting};

use super::model::RoomChannels;

const NAVIGATION_WIDTH: usize = 20;

pub struct View {
    terminal: Terminal,

    navigation_panel: Panel,
    message_panel: Panel,
    message_input_panel: Panel,
    status_panel: Panel,

    message_input: String,
    command_input: String,
}

impl View {
    pub fn new() -> Self {
        let terminal = Terminal::init();
        let terminal_size = terminal.panel.get_size();

        Self {
            terminal,

            navigation_panel: Panel::new((NAVIGATION_WIDTH, terminal_size.1 - 3), (1, 1)),
            message_panel: Panel::new((terminal_size.0 - NAVIGATION_WIDTH - 3, terminal_size.1 - 5), (NAVIGATION_WIDTH + 2, 1)),
            message_input_panel: Panel::new((terminal_size.0 - NAVIGATION_WIDTH - 3, 1), (NAVIGATION_WIDTH + 2, terminal_size.1 - 2 - 1)),
            status_panel: Panel::new((terminal_size.0, 1), (0, terminal_size.1 - 1)),

            message_input: String::new(),
            command_input: String::new(),
        }
    }

    pub fn update(&mut self, messages: &[Message], users: &HashMap<UserId, User>, rooms: &[RoomChannels]) {
        let terminal_size = self.terminal.panel.get_size();

        for (x_range, y) in [
            (1..=NAVIGATION_WIDTH, 0),
            (NAVIGATION_WIDTH + 2..=terminal_size.0 - 1 - 1, 0),
            (1..=NAVIGATION_WIDTH, terminal_size.1 - 1 - 1),
            (NAVIGATION_WIDTH + 2..=terminal_size.0 - 1 - 1, terminal_size.1 - 1 - 1),
            (NAVIGATION_WIDTH + 2..=terminal_size.0 - 1 - 1, terminal_size.1 - 3 - 1),
        ] {
            for x in x_range {
                self.terminal.panel.set_cursor((x, y));
                self.terminal.panel.print_char('─');
            }
        }
        for (x, y_range) in [
            (0, 1..=terminal_size.1 - 2 - 1),
            (NAVIGATION_WIDTH + 1, 1..=terminal_size.1 - 4 - 1),
            (NAVIGATION_WIDTH + 1, terminal_size.1 - 2 - 1..=terminal_size.1 - 2 - 1),
            (terminal_size.0 - 1, 1..=terminal_size.1 - 4 - 1),
            (terminal_size.0 - 1, terminal_size.1 - 2 - 1..=terminal_size.1 - 2 - 1)
        ] {
            for y in y_range {
                self.terminal.panel.set_cursor((x, y));
                self.terminal.panel.print_char('│');
            }
        }
        self.terminal.panel.set_cursor((0, 0));
        self.terminal.panel.print_char('┌');
        self.terminal.panel.set_cursor((terminal_size.0 - 1, 0));
        self.terminal.panel.print_char('┐');
        self.terminal.panel.set_cursor((0, terminal_size.1 - 1 - 1));
        self.terminal.panel.print_char('└');
        self.terminal.panel.set_cursor((terminal_size.0 - 1, terminal_size.1 - 1 - 1));
        self.terminal.panel.print_char('┘');
        self.terminal.panel.set_cursor((NAVIGATION_WIDTH + 1, terminal_size.1 - 1 - 1));
        self.terminal.panel.print_char('┴');
        self.terminal.panel.set_cursor((NAVIGATION_WIDTH + 1, 0));
        self.terminal.panel.print_char('┬');
        self.terminal.panel.set_cursor((NAVIGATION_WIDTH + 1, terminal_size.1 - 3 - 1));
        self.terminal.panel.print_char('├');
        self.terminal.panel.set_cursor((terminal_size.0 - 1, terminal_size.1 - 3 - 1));
        self.terminal.panel.print_char('┤');

        self.navigation_panel.clear();
        for room in rooms {
            self.navigation_panel.print_str(&format!("▼ {}\n", room.room.name));

            let mut channels_iter = room.channels.iter().peekable();
            while let Some(channel) = channels_iter.next() {
                let branch_char = if channels_iter.peek().is_some() {
                    '├'
                } else {
                    '└'
                };
                self.navigation_panel.print_str(&format!("{}╴#{}\n", branch_char, channel.name));
            }
        }

        self.message_panel.clear();
        let message_panel_size = self.message_panel.get_size();
        let mut message_line_count = 0;
        for message in messages.iter().rev() {
            const TIME_LENGTH: usize = 5;
            const USERNAME_LENGTH: usize = 10;
            const NON_CONTENT_LENGTH: usize = TIME_LENGTH + 1 + USERNAME_LENGTH + 1;

            let user = users.get(&message.user_id).unwrap();
            let nickname_color = match user.id.0.as_u128() % 12 {
                0 => Color16::Red,
                1 => Color16::Green,
                2 => Color16::Yellow,
                3 => Color16::Blue,
                4 => Color16::Magenta,
                5 => Color16::Cyan,
                6 => Color16::BrightRed,
                7 => Color16::BrightGreen,
                8 => Color16::BrightYellow,
                9 => Color16::BrightBlue,
                10 => Color16::BrightMagenta,
                11 => Color16::BrightCyan,
                _ => unreachable!(),
            };

            let mut lines = Vec::new();
            let mut current = message.content.as_str();
            while !current.is_empty() {
                let (line, rest) = current.split_at(cmp::min(message_panel_size.0 - NON_CONTENT_LENGTH, current.len()));
                lines.push(line);
                current = rest;
            }

            for line in lines.iter().rev() {
                if message_line_count >= message_panel_size.1 {
                    break;
                }
                self.message_panel.set_cursor((NON_CONTENT_LENGTH, message_panel_size.1 - 1 - message_line_count));
                self.message_panel.print_str(line);
                message_line_count += 1;
            }

            self.message_panel.set_cursor((0, message_panel_size.1 - 1 - (message_line_count - 1)));
            self.message_panel.set_formatting(Formatting { bold: false, italic: false, underline: false, reverse: false, foreground_color: Color::Color16(Color16::BrightBlack), background_color: Color::Default });
            self.message_panel.print_str(&message.created_time.with_timezone(&Local).format("%R").to_string());
            self.message_panel.set_cursor((TIME_LENGTH + 1, message_panel_size.1 - 1 - (message_line_count - 1)));
            self.message_panel.set_formatting(Formatting { bold: false, italic: false, underline: false, reverse: false, foreground_color: Color::Color16(nickname_color), background_color: Color::Default });
            self.message_panel.print_str(&format!("{:USERNAME_LENGTH$}", user.name));
            self.message_panel.set_formatting(Formatting::default());
        }

        self.message_input_panel.clear();
        self.message_input_panel.print_str(&self.message_input);

        self.status_panel.clear();
        self.status_panel.print_str(&format!(":{}", self.command_input));

        self.terminal.draw_panel(&self.navigation_panel);
        self.terminal.draw_panel(&self.message_panel);
        self.terminal.draw_panel(&self.message_input_panel);
        self.terminal.draw_panel(&self.status_panel);
        self.terminal.update();
    }

    pub fn end(&self) {
        self.terminal.end();
    }

    pub fn update_message_input(&mut self, message_input: String) {
        self.message_input = message_input;
    }

    pub fn update_command_input(&mut self, command_input: String) {
        self.command_input = command_input;
    }
}
