use std::{mem, collections::HashMap};

use nekochat::api::{RoomId, ChannelId};
use parsa::{CommandIdRef, CommandId, Command};
use tokio::{io::AsyncReadExt, sync::mpsc::{self, Receiver}, select};
use tuja::input::{Decoder, Input, InputKind};
use uuid::Uuid;

use super::{model::Model, view::View};

const MESSAGE_BUFFER: usize = 8;
const INPUT_BUFFER: usize = 3;

pub struct Controller {
    input_receiver: Receiver<Input>,
    context: Context,

    command_tree: HashMap<CommandId, Command>,

    message_input: String,
    command_input: String,
}

impl Controller {
    pub fn new() -> Self {
        let (input_sender, input_receiver) = mpsc::channel(MESSAGE_BUFFER);
        tokio::spawn(async move {
            let mut decoder = Decoder::new();
            let mut bytes = [0; INPUT_BUFFER];
            loop {
                let bytes_read = tokio::io::stdin().read(&mut bytes).await.unwrap();
                decoder.decode(&bytes[0..bytes_read], bytes_read);
                while let Some(char) = decoder.get_input() {
                    input_sender.send(char).await.unwrap();
                }
            }
        });

        Self {
            input_receiver,
            context: Context::MessageInput,

            command_tree: HashMap::from([
                (CommandId::Name(String::from("create-channel")), Command {
                    arguments: HashMap::from([
                        (String::from("room"), parsa::Argument {
                            kind: parsa::ArgumentKind::Positional { position: 0 },
                            value: Some(parsa::ValueKind::String),
                            optional: false,
                        }),
                        (String::from("name"), parsa::Argument {
                            kind: parsa::ArgumentKind::Positional { position: 1 },
                            value: Some(parsa::ValueKind::String),
                            optional: false,
                        }),
                    ]),
                    commands: HashMap::new(),
                }),
                (CommandId::Name(String::from("set-channel")), Command {
                    arguments: HashMap::from([
                        (String::from("channel"), parsa::Argument {
                            kind: parsa::ArgumentKind::Positional { position: 0 },
                            value: Some(parsa::ValueKind::String),
                            optional: false,
                        }),
                    ]),
                    commands: HashMap::new(),
                }),
            ]),

            message_input: String::new(),
            command_input: String::new(),
        }
    }

    pub async fn handle_input(&mut self, model: &mut Model, view: &mut View) -> bool {
        select! {
            Some(input) = self.input_receiver.recv() => match input.inner {
                InputKind::Char('\x03') => return true,
                InputKind::Char('\x7f') => match self.context {
                    Context::MessageInput => { self.message_input.pop(); },
                    Context::CommandInput => { self.command_input.pop(); },
                },
                InputKind::Char('\x1b') => match self.context {
                    Context::CommandInput => {
                        self.command_input.clear();
                        self.context = Context::MessageInput;
                    },
                    _ => (),
                },
                InputKind::Char('\r') => match self.context {
                    Context::MessageInput => {
                        model.send_message(mem::replace(&mut self.message_input, String::new())).await;
                    },
                    Context::CommandInput => {
                        let arguments = parsa::split_arguments(&self.command_input).unwrap();
                        let parsed_command = parsa::parse_arguments(&self.command_tree, &arguments).unwrap();

                        match parsed_command.command[..] {
                            [CommandIdRef::Name("create-channel")] => {
                                let name = parsed_command.arguments["name"].try_as_str().unwrap();
                                let room_id = RoomId(Uuid::parse_str(
                                    parsed_command.arguments["room"].try_as_str().unwrap()
                                ).unwrap());
                                model.create_channel(room_id, name.to_string()).await;
                            },
                            [CommandIdRef::Name("set-channel")] => {
                                let channel_id = ChannelId(Uuid::parse_str(
                                    parsed_command.arguments["channel"].try_as_str().unwrap()
                                ).unwrap());
                                model.load_channel(channel_id).await;
                            },
                            _ => (),
                        }

                        self.command_input.clear();
                        self.context = Context::MessageInput;
                    },
                },
                InputKind::Char(';') if input.alt => self.context = Context::CommandInput,
                InputKind::Char(char) => match self.context {
                    Context::MessageInput => self.message_input.push(char),
                    Context::CommandInput => self.command_input.push(char),
                },
                _ => (),
            },
            _ = model.handle_event() => (),
        }

        view.update_command_input(self.command_input.clone());
        view.update_message_input(self.message_input.clone());

        false
    }
}

enum Context {
    MessageInput,
    CommandInput,
}
