use std::collections::HashMap;

use nekochat::{client::Client, api::{event::Event, ChannelId, Message, UserId, User, RoomId, SessionToken, Room, Channel}};
use tokio::sync::mpsc::Receiver;
use uuid::Uuid;

use super::view::View;

pub struct Model {
    client: Client,
    event_receiver: Receiver<Event>,

    channel: ChannelId,
    messages: Vec<Message>,
    users: HashMap<UserId, User>,
    rooms: Vec<RoomChannels>,
}

impl Model {
    pub async fn new(server: &str, token: SessionToken, channel: ChannelId) -> Self {
        let mut client = Client::new(&server).await;
        client.login(token).await.unwrap();
        let event_receiver = client.event_receiver().unwrap();

        let mut rooms = Vec::new();
        for room in client.get_rooms().await.unwrap() {
            let channels = room.id.get_channels(&client).await.unwrap();
            rooms.push(RoomChannels { room, channels });
        }

        let mut model = Self {
            client,
            event_receiver,

            channel: ChannelId(Uuid::from_u128(0)),
            messages: Vec::new(),
            users: HashMap::new(),
            rooms,
        };

        model.load_channel(channel).await;

        model
    }

    pub async fn handle_event(&mut self) {
        let event = self.event_receiver.recv().await;
        match event.unwrap() {
            Event::MessageCreated { message } => {
                if message.channel_id != self.channel {
                    return;
                }

                self.users.entry(message.user_id).or_insert(message.user_id.get(&self.client).await.unwrap());
                self.messages.push(message);
            },
            Event::ChannelCreated { .. } => (),
        }
    }

    pub async fn send_message(&mut self, message: String) {
        self.channel.send(&self.client, message).await.unwrap();
    }

    pub fn update_view(&self, view: &mut View) {
        view.update(&self.messages, &self.users, &self.rooms);
    }

    pub async fn load_channel(&mut self, channel: ChannelId) {
        self.channel = channel;
        self.messages = channel.get_messages(&self.client).await.unwrap();

        self.users.clear();
        for message in &self.messages {
            self.users.entry(message.user_id).or_insert(message.user_id.get(&self.client).await.unwrap());
        }
    }

    pub async fn create_channel(&mut self, room_id: RoomId, name: String) {
        room_id.create_channel(&self.client, name).await.unwrap();
    }
}

pub struct RoomChannels {
    pub room: Room,
    pub channels: Vec<Channel>,
}
