use std::fs::File;

use nekochat::api::{SessionToken, ChannelId};
use serde::Deserialize;
use ui::{model::Model, view::View, controller::Controller};
use uuid::Uuid;

mod ui;

#[derive(Deserialize)]
struct Config {
    server: String,
    channel: Uuid,
    token: SessionToken,
}

#[tokio::main]
async fn main() {
    let config: Config = serde_json::from_reader(
        File::open("config.json").unwrap()
    ).unwrap();

    let mut model = Model::new(&config.server, config.token, ChannelId(config.channel)).await;
    let mut view = View::new();
    let mut controller = Controller::new();
    loop {
        model.update_view(&mut view);
        if controller.handle_input(&mut model, &mut view).await {
            break;
        }
    }
    view.end();
}
